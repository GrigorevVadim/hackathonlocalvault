﻿using HackathonLocalVault.Core.FileContentManager;
using HackathonLocalVault.Windows;
using Microsoft.Win32;
using System.IO;
using System.Windows;

namespace HackathonLocalVault
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly IFileContentManager fileContentManager;
        private string filePath = string.Empty;
        private string importFilePath = string.Empty;

        public MainWindow(IFileContentManager fileContentManager)
        {
            InitializeComponent();
            this.fileContentManager = fileContentManager;
        }

        private void mnuOpen_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                filePath = openFileDialog.FileName;
                fileContentManager.InitContent(File.ReadAllText(openFileDialog.FileName));
            }
        }

        private void mnuSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(filePath))
                return;

            File.WriteAllText(filePath, fileContentManager.GetContent());
        }

        private void mnuSaveAs_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(importFilePath))
                return;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "(*.json)|*.json";
            saveFileDialog.DefaultExt = "json";
            if (saveFileDialog.ShowDialog() == true)
            {
                filePath = saveFileDialog.FileName;
                File.WriteAllText(saveFileDialog.FileName, fileContentManager.GetContent());
            }
        }

        private void mnuExport_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(filePath))
                return;
            var password = string.Empty;
            PasswordInputBox inputDialog = new PasswordInputBox();
            if (inputDialog.ShowDialog() == true)
                password = inputDialog.Answer;

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "(*.csif)|*.csif";
            saveFileDialog.DefaultExt = "csif";
            if (saveFileDialog.ShowDialog() == true)
            {
                File.WriteAllText(saveFileDialog.FileName, fileContentManager.EncryptContent(password));
            }
        }

        private void mnuImport_Click(object sender, RoutedEventArgs e)
        {
            var password = string.Empty;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "(*.csif)|*.csif";
            openFileDialog.DefaultExt = "csif";
            if (openFileDialog.ShowDialog() == true)
            {
                importFilePath = openFileDialog.FileName;
                fileContentManager.InitEncryptedContent(File.ReadAllText(openFileDialog.FileName));
            }

            PasswordInputBox inputDialog = new PasswordInputBox();
            if (inputDialog.ShowDialog() == true)
                password = inputDialog.Answer;

            fileContentManager.DecryptContent(password);
        }
    }
}