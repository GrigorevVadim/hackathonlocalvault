using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace HackathonLocalVault.MVVM
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        protected void SetProperty<T>(ref T property, T value, [CallerMemberName] string propertyName = null)
        {
            property = value;
            NotifyPropertyChanged(propertyName);
        }
        
        public event PropertyChangedEventHandler PropertyChanged;  
  
        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = null)  
        {  
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        } 
    }
}