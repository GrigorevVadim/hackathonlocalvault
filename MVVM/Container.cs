using HackathonLocalVault.Core;
using HackathonLocalVault.Core.FileContentManager;
using HackathonLocalVault.ViewModel;
using Microsoft.Extensions.DependencyInjection;

namespace HackathonLocalVault.MVVM
{
    public static class Container
    {
        private static ServiceProvider _serviceProvider;

        public static ServiceProvider GetServiceProvider()
        {
            if (_serviceProvider == null) 
                InitContainer();

            return _serviceProvider;
        }

        private static void InitContainer()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<MainViewModel>();
            services.AddSingleton<MainWindow>();
            services.AddSingleton<IFileContentManager, FileContentManager>();
            services.AddSingleton<ICryproManager, CryptoManager>();
            services.AddSingleton<IFileContentProcessor, FileContentProcessor>();
        }
    }
}