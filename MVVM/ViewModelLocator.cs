using HackathonLocalVault.ViewModel;
using Microsoft.Extensions.DependencyInjection;

namespace HackathonLocalVault.MVVM
{
    public class ViewModelLocator
    {
        public MainViewModel MainViewModel
        {
            get { return Container.GetServiceProvider().GetService<MainViewModel>();}
        }
    }
}