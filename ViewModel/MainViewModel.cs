using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using HackathonLocalVault.Core.FileContentManager;
using HackathonLocalVault.Models;
using HackathonLocalVault.MVVM;
using HackathonLocalVault.ViewModel.SelectedLists;

namespace HackathonLocalVault.ViewModel
{
    public class MainViewModel : ViewModelBase
    {
        private readonly IFileContentManager fileContentManager;
        private SecretElement selectedElement;
        private SecretElement _selectedSecret;
        private SecretsContainer _selectedContainer;
        private Mount _selectedMount;
        private bool visibilitySecrets;
        private bool visibilityContainers;
        private bool visibilityMounts;
        private string _elementName = string.Empty;
        private string _elementValue = string.Empty;
        private SecretsStorage _storage;
        public ObservableCollection<SecretElement> SecretElements { get; set; }
        public ObservableCollection<SecretsContainer> SecretsContainers { get; set; }
        public ObservableCollection<Mount> Mounts { get; set; }

        public MainViewModel(IFileContentManager fileContentManager)
        {
            this.fileContentManager = fileContentManager;
            SecretElements = new ObservableCollection<SecretElement>();
            SecretsContainers = new ObservableCollection<SecretsContainer>();
            Mounts = new ObservableCollection<Mount>();

            fileContentManager.ContentUpdated += FileContentManagerOnContentUpdated;
        }

        public SecretElement SelectedSecret
        {
            get => _selectedSecret;
            set
            {
                SetProperty(ref _selectedSecret, value);
                ElementName = _selectedSecret.Name;
                ElementValue = _selectedSecret.Value;
            }
        }
        
        public SecretsContainer SelectedContainer
        {
            get => _selectedContainer;
            set
            {
                SetProperty(ref _selectedContainer, value);
                ElementName = _selectedContainer.Name;
            }
        }
        
        public Mount SelectedMount
        {
            get => _selectedMount;
            set
            {
                SetProperty(ref _selectedMount, value);
                ElementName = _selectedMount.Name;
            }
        }

        public string ElementName
        {
            get => _elementName;
            set => SetProperty(ref _elementName, value);
        }
        
        public string ElementValue
        {
            get => _elementValue;
            set => SetProperty(ref _elementValue, value);
        }

        public bool VisibilitySecrets
        {
            get => visibilitySecrets;
            set => SetProperty(ref visibilitySecrets, value);
        }

        public bool VisibilityContainers
        {
            get => visibilityContainers;
            set => SetProperty(ref visibilityContainers, value);
        }

        public bool VisibilityMounts
        {
            get => visibilityMounts;
            set => SetProperty(ref visibilityMounts, value);
        }
        
        public ICommand AddCommand => new SimpleCommand(() =>
        {
            if (VisibilityMounts)
            {
                MountListCommands.Add(_storage, ElementName);
                UpdateMountsCollection();
            }
            else if (VisibilityContainers)
            {
                ContainerListCommands.Add(_storage, SelectedMount.Id, ElementName);
                UpdateContainersCollection();
            }
            else if (VisibilitySecrets) 
            {
                SecretListCommands.Add(_storage, SelectedMount.Id, SelectedContainer.Id, ElementName, ElementValue);
                UpdateSecretsCollection();
            }
        });

        public ICommand RemoveCommand => new SimpleCommand(() =>
        {
            if (VisibilityMounts && SelectedMount != null)
            {
                MountListCommands.Remove(_storage, SelectedMount);
                UpdateMountsCollection();
            }
            else if (VisibilityContainers && SelectedContainer != null)
            {
                ContainerListCommands.Remove(_storage, SelectedMount.Id, SelectedContainer);
                UpdateContainersCollection();
            }
            else if (VisibilitySecrets && SelectedSecret != null)
            {
                SecretListCommands.Remove(_storage, SelectedMount.Id, SelectedContainer.Id, SelectedSecret);
                UpdateSecretsCollection();
            }
        });
        
        public ICommand SaveCommand => new SimpleCommand(() =>
        {
            if (VisibilityMounts && SelectedMount != null)
            {
                MountListCommands.Update(_storage, SelectedMount, ElementName);
                UpdateMountsCollection();
            }
            else if (VisibilityContainers && SelectedContainer != null)
            {
                ContainerListCommands.Update(_storage, SelectedMount.Id, SelectedContainer, ElementName);
                UpdateContainersCollection();
            }
            else if (VisibilitySecrets && SelectedSecret != null)
            {
                SecretListCommands.Update(_storage, SelectedMount.Id, SelectedContainer.Id, SelectedSecret, ElementName, ElementValue);
                UpdateSecretsCollection();
            }
        });
        
        public ICommand ForwardClickCommand => new SimpleCommand(() =>
        {
            if (VisibilityMounts && SelectedMount != null)
            {
                TurnOffListsVisibility();
                VisibilityContainers = true;
                UpdateContainersCollection();
            }
            else if (VisibilityContainers && SecretsContainers != null)
            {
                TurnOffListsVisibility();
                VisibilitySecrets = true;
                UpdateSecretsCollection();
            }
        });
        
        public ICommand BackClickCommand => new SimpleCommand(() =>
        {
            if (VisibilitySecrets)
            {
                TurnOffListsVisibility();
                VisibilityContainers = true;
                UpdateContainersCollection();
                
            }
            else if (VisibilityContainers)
            {
                TurnOffListsVisibility();
                VisibilityMounts = true;
                UpdateMountsCollection();
            }
        });

        private void FileContentManagerOnContentUpdated(object sender, EventArgs e)
        {
            Mounts.Clear();
            _storage = fileContentManager.GetCurrentSecretStorage();
            foreach (var mount in _storage.Mounts) 
                Mounts.Add(mount);
            TurnOffListsVisibility();
            VisibilityMounts = true;
        }

        private void TurnOffListsVisibility()
        {
            VisibilityMounts = VisibilityContainers = VisibilitySecrets = false;
        }

        private void UpdateMountsCollection()
        {
            Mounts.Clear();
            foreach (var mount in _storage.Mounts)
                Mounts.Add(mount);
            fileContentManager.SetCurrentSecretStorage(_storage);
        }

        private void UpdateContainersCollection()
        {
            SecretsContainers.Clear();
            var containers = _storage.Mounts.First(x => x.Id == SelectedMount.Id).SecretsContainers;
            foreach (var container in containers)
                SecretsContainers.Add(container);
            fileContentManager.SetCurrentSecretStorage(_storage);
        }

        private void UpdateSecretsCollection()
        {
            SecretElements.Clear();
            var secrets = _storage.Mounts
                .First(x => x.Id == SelectedMount.Id).SecretsContainers
                .First(x => x.Id == SelectedContainer.Id).Secrets;
            foreach (var secret in secrets)
                SecretElements.Add(secret);
            fileContentManager.SetCurrentSecretStorage(_storage);
        }
    }
}
