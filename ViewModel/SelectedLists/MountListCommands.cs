using System.Linq;
using HackathonLocalVault.Models;

namespace HackathonLocalVault.ViewModel.SelectedLists
{
    public static class MountListCommands
    {
        public static void Add(SecretsStorage storage, string name)
        {
            var mounts = storage.Mounts;
            var id = mounts.Max(x => x.Id);
            var mountsList = mounts.ToList();
            mountsList.Add(new Mount() {Id = ++id, Name = name, SecretsContainers = new SecretsContainer[0]});
            storage.Mounts = mountsList.ToArray();
        }

        public static void Remove(SecretsStorage storage, Mount mount)
        {
            var mounts = storage.Mounts;
            var mountsList = mounts.ToList();
            mountsList.Remove(mount);
            storage.Mounts = mountsList.ToArray();
        }

        public static void Update(SecretsStorage storage, Mount selectedMount, string name)
        {
            var mounts = storage.Mounts;
            foreach (var mount in mounts)
            {
                if (mount.Id == selectedMount.Id) 
                    mount.Name = name;
            }
        }
    }
}