using System.Linq;
using HackathonLocalVault.Models;

namespace HackathonLocalVault.ViewModel.SelectedLists
{
    public static class ContainerListCommands
    {
        public static void Add(SecretsStorage storage, int mountId,string name)
        {
            var containers = storage.Mounts.First(x => x.Id == mountId).SecretsContainers;
            var id = containers.Max(x => x.Id);
            var containersList = containers.ToList();
            containersList.Add(new SecretsContainer() {Id = id++, Name = name, Secrets = new SecretElement[0]});
            storage.Mounts.First(x => x.Id == mountId).SecretsContainers = containersList.ToArray();
        }

        public static void Remove(SecretsStorage storage, int mountId, SecretsContainer container)
        {
            var containers = storage.Mounts.First(x => x.Id == mountId).SecretsContainers;
            var containersList = containers.ToList();
            containersList.Remove(container);
            storage.Mounts.First(x => x.Id == mountId).SecretsContainers = containersList.ToArray();
        }

        public static void Update(SecretsStorage storage, int mountId, SecretsContainer selectedContainer, string name)
        {
            var containers = storage.Mounts.First(x => x.Id == mountId).SecretsContainers;
            foreach (var container in containers)
            {
                if (container.Id == selectedContainer.Id) 
                    container.Name = name;
            }
        }
    }
}