using System.Linq;
using HackathonLocalVault.Models;

namespace HackathonLocalVault.ViewModel.SelectedLists
{
    public static class SecretListCommands
    {
        public static void Add(SecretsStorage storage, int mountId, int containerId, string name, string value)
        {
            var secrets = storage.Mounts
                .First(x => x.Id == mountId).SecretsContainers
                .First(x => x.Id == containerId).Secrets;
            var id = secrets.Max(x => x.Id);
            var secretElements = secrets.ToList();
            secretElements.Add(new SecretElement() {Id = id++, Name = name, Value = value});
            storage.Mounts
                .First(x => x.Id == mountId).SecretsContainers
                .First(x => x.Id == containerId).Secrets = secretElements.ToArray();
        }

        public static void Remove(SecretsStorage storage, int mountId, int containerId, SecretElement secretElement)
        {
            var secrets = storage.Mounts
                .First(x => x.Id == mountId).SecretsContainers
                .First(x => x.Id == containerId).Secrets;
            var secretElements = secrets.ToList();
            secretElements.Remove(secretElement);
            storage.Mounts
                .First(x => x.Id == mountId).SecretsContainers
                .First(x => x.Id == containerId).Secrets = secretElements.ToArray();
        }

        public static void Update(SecretsStorage storage, int mountId, int containerId, SecretElement selectedSecret, string name, string value)
        {
            var secrets = storage.Mounts
                .First(x => x.Id == mountId).SecretsContainers
                .First(x => x.Id == containerId).Secrets;
            foreach (var secret in secrets)
            {
                if (secret.Id == selectedSecret.Id)
                {
                    secret.Name = name;
                    secret.Value = value;
                }
            }
        }
    }
}