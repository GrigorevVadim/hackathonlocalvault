namespace HackathonLocalVault.Models
{
    public class SecretsStorage
    {
        public Mount[] Mounts;
    }

    public class Mount
    { 
        public int Id { get; set; }
        public string Name { get; set; }
        public SecretsContainer[] SecretsContainers { get; set; }
    }

    public class SecretsContainer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SecretElement[] Secrets { get; set; }
}

    public class SecretElement
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}