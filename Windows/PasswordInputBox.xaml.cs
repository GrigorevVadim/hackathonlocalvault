﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HackathonLocalVault.Windows
{
    /// <summary>
    /// Логика взаимодействия для PasswordInputBox.xaml
    /// </summary>
    public partial class PasswordInputBox : Window
    {
        public PasswordInputBox()
        {
            InitializeComponent();
        }

		private void btnDialogOk_Click(object sender, RoutedEventArgs e)
		{
			this.DialogResult = true;
		}

		private void Window_ContentRendered(object sender, EventArgs e)
		{
			pswBox.SelectAll();
			pswBox.Focus();
		}

		public string Answer
		{
			get { return pswBox.Password; }
		}
	}
}
