﻿using Rijndael256;
using Rijndael = Rijndael256.Rijndael;

namespace HackathonLocalVault.Core
{
    public interface ICryproManager
    {
        string EncryptWithPassword(string inputString, string password);
        string DecryptWithPassword(string inputString, string password);
    }
    public class CryptoManager : ICryproManager
    {
        public CryptoManager()
        { 
        }

        public string DecryptWithPassword(string inputString, string password)
        {
            return Rijndael.Decrypt(inputString, password, KeySize.Aes256);
        }

        public string EncryptWithPassword(string inputString, string password)
        {
            return Rijndael.Encrypt(inputString, password, KeySize.Aes256);
        }
    }
}
