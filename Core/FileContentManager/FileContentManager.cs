﻿using HackathonLocalVault.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HackathonLocalVault.Core.FileContentManager
{
    public interface IFileContentManager
    {
        void InitContent(string content);
        void InitEncryptedContent(string encryptedContent);
        SecretsStorage GetCurrentSecretStorage();
        void SetCurrentSecretStorage(SecretsStorage storage);
        string GetContent();
        string EncryptContent(string password);
        string DecryptContent(string password);
        event EventHandler ContentUpdated;
    }
    public class FileContentManager : IFileContentManager
    {
        private readonly ICryproManager cryproManager;
        private readonly IFileContentProcessor fileContentProcessor;

        private string encryptedContent;
        private string _content;

        public event EventHandler ContentUpdated;

        public FileContentManager(ICryproManager cryproManager, IFileContentProcessor fileContentProcessor)
        {
            this.cryproManager = cryproManager;
            this.fileContentProcessor = fileContentProcessor;
        }

        public string GetContent()
        {
            return content;
        }

        public SecretsStorage GetCurrentSecretStorage()
        {
            if (string.IsNullOrEmpty(content))
                return null;
            try
            {
                var result = fileContentProcessor.ParseFileContent(content);
                return result;
            }
            catch
            {
                return null;
            }
        }

        public void SetCurrentSecretStorage(SecretsStorage storage)
        {
            this._content = fileContentProcessor.GenerateFileContent(storage);
        }

        public void InitContent(string content)
        {
            this.content = content;
        }
        public void InitEncryptedContent(string encryptedContent)
        {
            this.encryptedContent = encryptedContent;
        }
        public string DecryptContent(string password)
        {
            if (string.IsNullOrEmpty(encryptedContent))
                return null;

            content = cryproManager.DecryptWithPassword(encryptedContent, password);
            return content;
        }

        public string EncryptContent(string password)
        {
            if (string.IsNullOrEmpty(content))
                return null;

            encryptedContent = cryproManager.EncryptWithPassword(content, password);
            return encryptedContent;
        }

        private string content
        {
            get => _content;
            set
            {
                _content = value;
                ContentUpdated?.Invoke(null, null);
            }
        }
    }
}
