﻿using HackathonLocalVault.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HackathonLocalVault.Core
{
    public interface IFileContentProcessor
    {
        SecretsStorage ParseFileContent(string fileContent);
        string GenerateFileContent(SecretsStorage storage);
    }
    public class FileContentProcessor : IFileContentProcessor
    {
        public SecretsStorage ParseFileContent(string fileContent)
        {
            return JsonConvert.DeserializeObject<SecretsStorage>(fileContent);
        }

        public string GenerateFileContent(SecretsStorage storage)
        {
            return JsonConvert.SerializeObject(storage);
        }
    }
}
